namespace DataLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class anotherchangeforme : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Customers", "UserName");
            DropColumn("dbo.Customers", "PassWord");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Customers", "PassWord", c => c.String(nullable: false, maxLength: 30));
            AddColumn("dbo.Customers", "UserName", c => c.String(nullable: false, maxLength: 30));
        }
    }
}
