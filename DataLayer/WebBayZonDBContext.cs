﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityFrameworkDB;
using System.Data.Entity;

namespace DataLayer
{
    public class WebBayZonDBContext : DbContext
    {
        public WebBayZonDBContext() : base("WebBayZonContext")
        {

        }

        public DbSet<Customer> Customers { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Product> Products { get; set; }
    }
}
