﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace EntityFrameworkDB
{
    public class Customer
    {
        public int CustomerID { get; set; }

        [Required][MaxLength(30)]
        public string FirstName { get; set; }

        [Required][MaxLength(30)]
        public string LastName { get; set; }

        public string HouseNumber_Name { get; set; }

        public string AddressLine1 { get; set; }

        public string AddressLine2 { get; set; }

        public string City { get; set; }

        public string County { get; set; }

        public string Postcode { get; set; }

        public bool IsAdmin { get; set; }
        
        public ICollection<Order> Orders { get; set; }
    }
}