﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityFrameworkDB
{
    public class Order
    {
        public int OrderID { get; set; }

        public int CustomerID { get; set; }

        public Customer Customer { get; set; }

        public ICollection<Product> Products { get; set; }

    }
}
