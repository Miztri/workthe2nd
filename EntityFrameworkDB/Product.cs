﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace EntityFrameworkDB
{
    public class Product
    {
        public int ProductID { get; set; }

        public string ProductName { get; set; }

        [MaxLength(256)]
        public string ProductDescription { get; set; }

        public decimal Price { get; set; }

        public string ProductDementions { get; set; }

        public int ProductInStock { get; set; }

        public bool InStock { get; set; }

        public string ProductLocation { get; set; }

        public int SellerID { get; set; }

        public ICollection<Order> Orders { get; set; }

    }
}
