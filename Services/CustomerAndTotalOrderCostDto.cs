﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class CustomerAndTotalOrderCostDto
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public decimal TotalCost { get; set; }

    }
}
