﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityFrameworkDB;
using DataLayer;
using System.ComponentModel;

namespace Services
{
    public class CustomerService : ICustomerService
    {
        WebBayZonDBContext context = new WebBayZonDBContext();

        public CustomerService()
        {

        }

        public object[] CustomerID { get; private set; }

        public void AddCustomer(Customer customer)
        {
            context.Customers.Add(customer);
            context.SaveChanges();
        }

        public void AddProduct(Product product)
        {
            context.Products.Add(product);
            context.SaveChanges();
        }

        public void DeleteCustomerByID(Customer customer)
        {
            Customer customerName = context.Customers.Find(customer.CustomerID);
            if (customerName != null)
            {
                context.Customers.Remove(customerName);
                context.SaveChanges();

            }
        }

        public void DeleteProductByID(int ProductID)
        {
            throw new NotImplementedException();
        }

        public List<Customer> GetAllCustomers()
        {
            List<Customer> customers;
            customers = context.Customers.ToList();
            return customers;
        }

        public List<Product> GetAllProducts()
        {
            List<Product> products;
            products = context.Products.ToList();
            return products;
        }

        public Customer GetCustomer(int CustomerID)
        {
            Customer customer;
            customer = context.Customers.Find(CustomerID);
            return customer;
        }

        public List<CustomerAndTotalOrderCostDto> GetCustomerAndTotalOrderCosts()
        {
            {
                List<CustomerAndTotalOrderCostDto> customers;
                customers = context.Customers.Select(

                    c => new CustomerAndTotalOrderCostDto()
                    {
                        FirstName = c.FirstName,
                        LastName = c.LastName,
                        TotalCost = c.Orders.Select(o => o.Products).Select(p => p.DefaultIfEmpty().Sum(pr => pr.Price)).DefaultIfEmpty().Sum()
                    }).ToList();
                return customers;
            }
        }

        public Product GetProduct(int ProductID)
        {
            Product product;
            product = context.Products.Find(ProductID);
            return product;
        }

        public void UpDateCustomer(Customer customer)
        {
            var customerIs = context.Customers.Find(customer.CustomerID);
            customerIs.CustomerID = customer.CustomerID;
            customerIs.FirstName = customer.FirstName;
            customerIs.LastName = customer.LastName;
            customerIs.HouseNumber_Name = customer.HouseNumber_Name;
            customerIs.AddressLine1 = customer.AddressLine1;
            customerIs.AddressLine2 = customer.AddressLine2;
            customerIs.City = customer.City;
            customerIs.County = customer.County;
            customerIs.Postcode = customer.Postcode;
            customerIs.IsAdmin = customer.IsAdmin;
            context.SaveChanges();
        }

        public void UpDateProduct(Product product)
        {
            var productIs = context.Products.Find(product.ProductID);
            productIs.ProductID = product.ProductID;
            productIs.ProductDescription = product.ProductDescription;
            productIs.Price = product.Price;
            productIs.ProductDementions = product.ProductDementions;
            productIs.ProductInStock = product.ProductInStock;
            productIs.ProductLocation = product.ProductLocation;
            productIs.SellerID = product.SellerID;
            productIs.InStock = product.InStock;
            context.SaveChanges();
        }
    }
}
