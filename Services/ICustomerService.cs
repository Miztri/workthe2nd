﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityFrameworkDB;

namespace Services
{
    public interface ICustomerService
    {
        List<Customer> GetAllCustomers();
        void AddCustomer(Customer customer);
        Customer GetCustomer(int CustomerID);
        void UpDateCustomer(Customer customer);
        void DeleteCustomerByID(Customer customer);

        List<Product> GetAllProducts();
        void AddProduct(Product product);
        Product GetProduct(int ProductID);
        void UpDateProduct(Product product);
        void DeleteProductByID(int ProductID);

        List<CustomerAndTotalOrderCostDto> GetCustomerAndTotalOrderCosts();
    }
}
